import express from 'express';
import dotenv from 'dotenv';
import morgan from 'morgan';
import cors from "cors";
import csrf from 'csurf';
import cookieParser from 'cookie-parser';
import indexRouter from './src/routes/index.js';
import { Relationship } from "./src/relationship/relationship.js";
import bodyParser from 'body-parser';
import fs from 'fs';
import multer from 'multer'
import http from 'http'
import { Server } from 'socket.io';
const upload = multer();

dotenv.config()
const app = express()
const server = http.createServer(app)
const port = process.env.PORT

// const io = new Server(server);
const io = new Server (server,{ 
    cors: {
      origin: '*'
    }
}) 

//in case server and client run on different urls
io.on('connection', (socket) => {
	socket.on("order", (data) => {
		socket.send(data);
		socket.emit('order', data)
		socket.broadcast.emit('order', data)
	})
});

app.use(morgan("dev"))

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// app.use(bodyParser.json());
// app.use(bodyParser.urlencoded({ extended: true }));

// app.use(express.static(__dirname + '/uploads'));
// app.use(upload.array());
app.use(express.static('public'));


/* Read models */
await fs.readdir('src/model', async (err, files) => {
	files.forEach((file, i) => {
		import('./src/model/' + file);
	})
})

/* Read model relationships */
await fs.readdir('src/relationship', async (err, files) => {
	await new Promise(r => setTimeout(r, process.env.RELATIONSHIP_TIMEOUT))
	new Relationship()
})

// Add cookie-parser middleware
app.use(cookieParser());

// // Add CSRF middleware
// app.use(csrf({ cookie: true }));

// Request origin allowed
app.use(cors({ credentials: true, origin: '*' }))

app.use('/assets', express.static('assets'))

//initialize /api in all route
app.use('/api', indexRouter)

server.listen(port, () => {
  console.log(`Yummy listening on port ${port}`)
})