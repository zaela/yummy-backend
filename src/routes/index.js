import { Router } from 'express';
import multer from 'multer';
import path from 'path';
import { timestampFilename } from '../../utils.js';
import { add_address_controller, detail_address_controller, edit_address_controller, get_address_controller } from '../controller/addressController.js';
import { csrf_token_controller, delete_users_controller, edit_profile_controller, edit_users_controller, get_detail_users_controller, get_users_controller, login_controller, login_mobile_controller, logout_controller, refresh_token_controller, register_controller, update_profile_picture_controller } from '../controller/authController.js';
import { add_products_controller, delete_products_controller, edit_products_controller, get_detail_product_controller, get_products_controller } from '../controller/productsController.js';
import { add_restaurants_controller, delete_restaurants_controller, edit_restaurants_controller, get_detail_restaurants_controller, get_restaurants_controller } from '../controller/restaurantsController.js';
import { add_transactions_controller, cancel_transactions_controller, checkout_transactions_controller, complete_transactions_controller, delete_transactions_controller, edit_transactions_controller, get_detail_transactions_controller, get_detail_transactions_mobile_controller, get_transactions_controller, get_transactions_mobile_controller } from '../controller/transactionsController.js';
import { verifyToken } from '../middlewares/authToken.js';
import { report_pdf_controller } from '../controller/reportController.js';

const router = Router()

//MULTER
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, 'assets/storages')
    },
    filename: function (req, file, cb) {
      cb(null, timestampFilename(file.originalname))
    }
  })

const storage1 = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, 'assets/restaurants')
    },
    filename: function (req, file, cb) {
      cb(null, timestampFilename(file.originalname))
    }
  })
const upload = multer({
    storage: storage,
    limits: {
      fileSize: 10097152
    },
    fileFilter: function (req, file, callback) {
      var ext = path.extname(file.originalname);
      if (ext !== '.png' && ext !== '.jpg' && ext !== '.jpeg') {
        return callback(new Error('Only images are allowed'));
      }
      callback(null, true);
    },
  })

const uploadRestaurant = multer({
  storage: storage1,
  limits: {
    fileSize: 10097152
  },
  fileFilter: function (req, file, callback) {
    var ext = path.extname(file.originalname);
    if (ext !== '.png' && ext !== '.jpg' && ext !== '.jpeg') {
      return callback(new Error('Only images are allowed'));
    }
    callback(null, true);
  },
})


/* User Authentication */
router.post('/user/refresh-token', refresh_token_controller)
router.get('/user/csrf-token', csrf_token_controller)
router.post('/user/login', login_controller)
router.post('/user/register', register_controller)
router.post('/user/logout', logout_controller)
router.get('/user/get', get_users_controller)
router.patch('/user/edit', edit_users_controller)
router.get('/user/detail', get_detail_users_controller)
router.delete('/user/delete', delete_users_controller)

/* User Mobile */
router.patch('/user-mobile/edit', edit_profile_controller)
router.post('/user-mobile/login', login_mobile_controller)
router.patch('/user-mobile/photo', [verifyToken, upload.single('profile_picture')], update_profile_picture_controller)

// Restaurants
router.get('/restaurants/get', verifyToken, get_restaurants_controller)
router.get('/restaurants/get-by-user', verifyToken, get_restaurants_controller)
router.delete('/restaurants/delete', verifyToken, delete_restaurants_controller)
router.post('/restaurants/add', [verifyToken, uploadRestaurant.single('restaurant_image')], add_restaurants_controller)
router.patch('/restaurants/edit', [verifyToken, uploadRestaurant.single('restaurant_image')], edit_restaurants_controller)
router.get('/restaurants/detail', verifyToken, get_detail_restaurants_controller)

// Products
router.get('/products-mobile/get', get_products_controller)
// router.get('/products-mobile/get', get_products_controller)
router.get('/products/get', verifyToken, get_products_controller)
router.get('/products/detail', verifyToken, get_detail_product_controller)
router.post('/products/add', [verifyToken, upload.fields([{name:'variant_images', maxCount:10}, {name:'product_images', maxCount:10}])], add_products_controller)
router.patch('/products/edit', [verifyToken, upload.fields([{name:'variant_images', maxCount:10}, {name:'variant_images_new', maxCount:10}, {name:'product_images_new', maxCount:10}])], edit_products_controller)
router.delete('/products/delete', verifyToken, delete_products_controller)

// Transactions
router.get('/transactions/get', verifyToken, get_transactions_controller)
router.patch('/transactions/complete', verifyToken, complete_transactions_controller)
router.delete('/transactions/delete', verifyToken, delete_transactions_controller)

router.post('/transactions/checkout', verifyToken, checkout_transactions_controller)
router.patch('/transactions/cancel', verifyToken, cancel_transactions_controller)

// Transactions mobile
router.get('/transactions-mobile/get', verifyToken, get_transactions_mobile_controller)
router.get('/transactions-mobile/detail', verifyToken, get_detail_transactions_mobile_controller)
router.post('/transactions-mobile/add', verifyToken, add_transactions_controller)
router.patch('/transactions-mobile/edit', verifyToken, edit_transactions_controller)
router.delete('/transactions-mobile/delete', verifyToken, delete_transactions_controller)

// Address
router.get('/address/get', verifyToken, get_address_controller)
router.post('/address/add', add_address_controller)
router.get('/address/detail', verifyToken, detail_address_controller)
router.patch('/address/edit', verifyToken, edit_address_controller)

router.get('/laporanKunjungan', report_pdf_controller)

export default router