import { Op, Sequelize } from 'sequelize';
import db from '../config/index.js';
import conn from '../config/sqlz.js';
import ProductImagesModel from '../model/ProductImagesModel.js';
import ProductsModel from '../model/ProductsModel.js';
import ProductVariantsModel from '../model/ProductVariantsModel.js';
import RatingModel from '../model/RatingModel.js';
import TransactionsModel from '../model/TransactionsModel.js';

export const get_products_service = async (req, res) => {
    const {limit, offset} = req.query
    let clause = {
      subQuery: false,
      attributes: [
        "id",
        "restaurant_id",
        "name",
        "price",
        "description",
        "status",
        [
          Sequelize.fn("avg", Sequelize.col("ratings.rate")),
          "avgRate"
        ],
      ],
      group:["products.id", "product_variants.id", "product_images.id"]
    
    }
    let include = {
      include: [
        {
          model: ProductVariantsModel,
          attributes: ['variant_name', 'variant_image']
        },
        {
          model: ProductImagesModel,
          attributes: ['file_name']
        },
        {
          model: RatingModel,
          required:false,
          attributes: [],
        }
      ],
    }
    if (req.query.types) {
      switch (req.query.types) {
        case 'new_food':
          clause.order = [['createdAt', 'DESC']]
          break;
        case 'popular':
          include.include.push(
            {
              model: TransactionsModel,
              attributes: ['qty', 'status'],
            }
          )
          clause.attributes.push([
            Sequelize.fn("count", Sequelize.col("ratings.rate")),
            "totalQty"
          ])
          clause.group.push('transactions.id')
          clause.order = [['totalQty', 'DESC']]
          break;
        case 'recommended':
          clause.order = [['avgRate', 'DESC']]
          break;
      
        default:
          break;
      }
    }


    try {
      let products = await ProductsModel.findAll(
        {
          limit: Number(limit),
          offset: Number(offset * limit - limit),
          ...include,
          ...clause,
        }
      )
      
      // delete clause.limit;
      // delete clause.offset;
      let total = await ProductsModel.findAll(
        {
          ...include,
          ...clause,
        }
      )
      
      return res.status(200).send({
        status: true,
        data: products,
        page: Number(offset),
        total: Number(total?.length ?? 0),
        totalPage: Math.ceil(total.length/limit)
      })
  
    } catch (err) {
      return res.status(500).send({
        status: false,
        message: err.message
      })
    }
  }

export const get_detail_product_service = async (req, res) => {
    const {id} = req.query
    try {
      const product = await ProductsModel.findOne(
        {
          where: {id:id},
          include: [
            {
              model: ProductVariantsModel,
              attributes: ['id', 'variant_name', 'variant_image']
            },
            {
              model: ProductImagesModel,
              attributes: ['id','file_name']
            }
          ],
        }
      )
      return res.status(201).send({
        status: true,
        data: product
      })
  
    } catch (err) {
      return res.status(500).send({
        status: false,
        message: err.message
      })
    }
  }

export const add_products_service = async (req, res) => {
    // return true
    let {restaurant_id, name, description, variants, price, status} = req.body
    let { product_images, variant_images } = req.files
    const transactionInstance = await conn.transaction();

    try {
      let product = await ProductsModel.create({
        restaurant_id: restaurant_id,
        name: name,
        description: description,
        price: price,
        status: status,
      }, { transaction: transactionInstance })
      // .then(async product => {
      // })
      product_images = product_images.map((image) => {
        const file_name = `${image.destination}/${image.filename}`
        return { 
          product_id: product.id,
          file_name : file_name
        }
      })
  
      variants = variant_images.map((image, index) => {
        const file_name = `${image.destination}/${image.filename}`
        return { 
          product_id: product.id,
          variant_name: Array.isArray(variants) ? variants[index] : variants,
          variant_image: file_name,
        }
      })

      await ProductImagesModel.bulkCreate(product_images, { transaction: transactionInstance })
      await ProductVariantsModel.bulkCreate(variants, { transaction: transactionInstance })

      
      await transactionInstance.commit();
      return res.status(201).send({
        status: true,
        message: 'Insert Success'
      })
  
    } catch (err) {
      await transactionInstance.rollback();
      return res.status(500).send({
        status: false,
        message: err.message
      })
    }
  }

export const edit_products_service = async (req, res) => {
  let {product_id, name, restaurant_id, description, price, status, variants, variant_images_id, product_images_delete, variants_delete} = req.body
  
  let { product_images_new, variant_images, variant_images_new } = req.files
  const clause = { id: product_id }
    try {
      await ProductsModel.update({
        restaurant_id: restaurant_id,
        name: name,
        description: description,
        price: price,
        status: status,
      },
      {
        where: clause
      })

      if (product_images_new) {
        product_images_new = product_images_new.map((image) => {
          const file_name = `${image.destination}/${image.filename}`
          return { 
            product_id: product_id,
            file_name : file_name
          }
        })
        // add new product images
        await ProductImagesModel.bulkCreate(product_images_new)
      }

      if (product_images_delete) {
        // Delete product image
        if (Array.isArray(product_images_delete)) {
          for (let index = 0; index < product_images_delete.length; index++) {
            ProductImagesModel.destroy({
              where: {
                id: product_images_delete[index]
              }
            })
          }
        }else{
          ProductImagesModel.destroy({
            where: {
              id: product_images_delete
            }
          })
        }
      }

      // variants = variant_images.map((image, index) => {
      //   const file_name = `${image.destination}/${image.filename}`
      //   return { 
      //     product_id: product.id,
      //     variant_name: Array.isArray(variants) ? variants[index] : variants,
      //     variant_image: file_name,
      //   }
      // })

      
      // console.log("variants :", JSON.parse(variants))
      const dataVariant = JSON.parse(variants)
      const oldVariant = dataVariant.filter((item) => item.id !== "")
      const newVariant = dataVariant.filter((item) => item.id === "")
      console.log("oldVariant:", oldVariant)
      console.log("newVariant:", newVariant)
      console.log("variant_images:", variant_images)
      // variants = JSON.parse(variants)
      if (variants?.length) {
        // JSON.parse(variants).map((variant, index) => {
        //   if (variant.id !== '') { //cek apakah variant baru
        //     let payload;
        //     console.log('JSON.parse(variant_images) :', variant_images)
        //     console.log('JSON.parse(variant_images_id) :', variant_images_id)
        //     // let variantImage = JSON.parse(variant_images).find((item) => item.id == variant.id)

        //     if (variant.id == variant_images_id) {
        //       const file_name = `${variant_images[index].destination}/${variant_images[index].filename}`
        //       payload={
        //         variant_name: variant.variant_name,
        //         variant_image: file_name,
        //       }
        //     } else {
        //       payload={
        //         variant_name: variant.variant_name,
        //       }
        //     }
        //     ProductVariantsModel.update(payload,{
        //       where:{ id: variant.id }
        //     })
        //   }else{
        //     const file_name = `${variant_images[index].destination}/${variant_images[index].filename}`
        //     const productVariant = { 
        //       product_id: product_id,
        //       variant_name: variant.variant_name,
        //       variant_image: file_name,
        //     }
        //     ProductVariantsModel.create(productVariant)
        //   }
        // })

        if (oldVariant) {
          let payload;

          oldVariant.map((variant, index) => {
            // if (variant.id == variant_images_id) {
            //   const file_name = `${variant_images[index].destination}/${variant_images[index].filename}`
            //   payload={
            //     variant_name: variant.variant_name,
            //     variant_image: file_name,
            //   }
            // } else {
            //   payload={
            //     variant_name: variant.variant_name,
            //   }
            // }
            payload={
              variant_name: variant.variant_name,
            }

            ProductVariantsModel.update(payload,{
              where:{ id: variant.id }
            })
          })

          if (variant_images) {
            let payload;
            variant_images.map((item, index) => {
              const file_name = `${variant_images[index].destination}/${variant_images[index].filename}`
              payload={
                variant_image: file_name,
              }
              
              ProductVariantsModel.update(payload,{
                where:{ id: variant_images_id }
              })
            })
          }
        }
        
        if (newVariant) {
          newVariant.map((variant, index) => {
            const file_name = `${variant_images_new[index].destination}/${variant_images_new[index].filename}`
            const productVariant = { 
              product_id: product_id,
              variant_name: variant.variant_name,
              variant_image: file_name,
            }
            ProductVariantsModel.create(productVariant)
          })
        }
      }


      // for (let index = 0; index < variants.length; index++) {
      //   console.log("variants[index].id :", variants)
      //   if (variants[index].id !== '') { //cek apakah variant baru
      //     const file_name = `${variant_images[index].destination}/${variant_images[index].filename}`
      //     await ProductVariantsModel.update({
      //       variant_name: variants[index].variant_name,
      //       // variant_image: variants[index].variant_image,
      //     },{
      //       where:{ id: variants[index].id }
      //     })
      //   }else{
      //     const file_name = `${variant_images[index].destination}/${variant_images[index].filename}`
      //     const productVariant = { 
      //       product_id: product.id,
      //       variant_name: Array.isArray(variants) ? variants[index] : variants,
      //       variant_image: file_name,
      //     }
      //     await ProductVariantsModel.create(productVariant)
      //   }
      // }
      
      // let product_variants = variants.map((variant_name, index) => {
      //   return {
      //     product_id: product_id,
      //     variant_name: variant_name,
      //     variant_image: `${variant_images_new[index].destination}/${variant_images_new[index].filename}`,
      //   }
      // })
      
      // add new variants
      // await ProductVariantsModel.bulkCreate(product_variants)

      console.log("variants_delete :", typeof(variants_delete))
      
      // Delete variants
      if (variants_delete?.length) {
        JSON.parse(variants_delete).map((variants_delete_id, index) => {
          ProductVariantsModel.destroy({
            where: {
              id: variants_delete_id
            }
          })
        })
      }
      
      return res.status(201).send({
        status: true,
        message: 'Edit Success'
      })
  
    } catch (err) {
      return res.status(500).send({
        status: false,
        message: err.message
      })
    }
  }

export const delete_products_service = async (req, res) => {
    const {id} = req.query
    try {
      await ProductsModel.destroy({
        where: { id: id }
      })
      return res.status(201).send({
        status: true,
        message: 'Delete Success'
      })
  
    } catch (err) {
      return res.status(500).send({
        status: false,
        message: err.message
      })
    }
  }