import db from '../config/index.js';
import RestaurantsModel from '../model/RestaurantsModel.js';

export const get_restaurants_service = async (req, res) => {
    let query = {
      order: [['createdAt', 'DESC']],
    }

    if (req.query.limit) {
      query = {
        ...query,
        limit: Number(req.query.limit),
        offset: Number(req.query.offset * req.query.limit - req.query.limit),
      }
    }

    try {
      // get restaurants
      const restaurants = await RestaurantsModel.findAll(query)

      if (req.query.limit) {
        // total restaurants
        delete query.limit;
        delete query.offset;
        const total = await RestaurantsModel.findAll(query)
  
        return res.status(200).send({
          status: true,
          data: restaurants,
          page: Number(req.query.offset),
          total: Number(total?.length ?? 0),
          totalPage:Math.ceil(total?.length/req.query.limit)
        })
      }

      return res.status(200).send({
        status: true,
        data: restaurants
      })
  
    } catch (err) {
      return res.status(500).send({
        status: false,
        message: err.message
      })
    }
  }

export const get_restaurants_by_user_service = async (req, res) => {
    const {user_id} = req.query
    try {
      // get restaurants
      const restaurants = await RestaurantsModel.findAll(
        {
          limit: Number(req.query.limit),
          offset: Number(req.query.offset * req.query.limit - req.query.limit),
          where: {user_id:user_id},
          order: [['createdAt', 'DESC']],
        }
      )

      return res.status(201).send({
        status: true,
        data: restaurants
      })
  
    } catch (err) {
      return res.status(500).send({
        status: false,
        message: err.message
      })
    }
  }

export const get_detail_restaurants_service = async (req, res) => {
    const {id} = req.query
    try {
      // get restaurants
      const restaurants = await RestaurantsModel.findOne(
        {
          where: {id:id}
        }
      )

      return res.status(200).send({
        status: true,
        data: restaurants
      })
  
    } catch (err) {
      return res.status(500).send({
        status: false,
        message: err.message
      })
    }
  }

export const add_restaurants_service = async (file, body, res) => {
    const {name, location, status, user_id} = body
    let restaurant_image = file
    const file_name = `${restaurant_image.destination}/${restaurant_image.filename}`
    try {
      await RestaurantsModel.create({
        user_id: user_id,
        restaurant_image: file_name,
        name: name,
        location: location,
        status: status,
      })
      return res.status(201).send({
        status: true,
        message: 'Insert Success'
      })
  
    } catch (err) {
      return res.status(500).send({
        status: false,
        message: err.message
      })
    }
  }

export const edit_restaurants_service = async (file, body, res) => {
    const {name, location, status, user_id, id} = body
    let restaurant_image = file
    let restaurant = {
      user_id: user_id,
      name: name,
      location: location,
      status: status,
    }

    if (restaurant_image) {
      const file_name = `${restaurant_image.destination}/${restaurant_image.filename}`
      restaurant = {
        ...restaurant,
        restaurant_image: file_name,
      }
    }
    try {
      await RestaurantsModel.update(restaurant,
      {
        where: {id:id}
      })
      return res.status(200).send({
        status: true,
        message: 'Edit Success'
      })
  
    } catch (err) {
      return res.status(500).send({
        status: false,
        message: err.message
      })
    }
  }

export const delete_restaurants_service = async (req, res) => {
    const {id} = req.query
    try {
      await RestaurantsModel.destroy({
        where: { id: id }
      })
      return res.status(200).send({
        status: true,
        message: 'Delete Success'
      })
  
    } catch (err) {
      return res.status(500).send({
        status: false,
        message: err.message
      })
    }
  }