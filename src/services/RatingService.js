import db from '../config/index.js';
import RatingModel from '../model/RatingModel.js';

export const get_rating_service = async (req, res) => {
    try {
      // get restaurants
      const restaurants = await RatingModel.findAll(
        {
          limit: Number(req.query.limit),
          offset: Number(req.query.offset * req.query.limit - req.query.limit),
          // where: where_clause,
          order: [['createdAt', 'DESC']],
        }
      )

      return res.status(201).send({
        status: true,
        data: restaurants.rows
      })
  
    } catch (err) {
      return res.status(500).send({
        status: false,
        message: err.message
      })
    }
  }

export const add_rating_service = async (req, res) => {
    const {transaction_id, rate} = req.body
    try {
      await RatingModel.create({
        transaction_id: transaction_id,
        rate: rate,
      })
      return res.status(201).send({
        status: true,
        message: 'Insert Success'
      })
  
    } catch (err) {
      return res.status(500).send({
        status: false,
        message: err.message
      })
    }
  }

export const edit_rating_service = async (req, res) => {
    const {transaction_id, rate} = req.body
    try {
      await RatingModel.update({
        transaction_id: transaction_id,
        rate: rate,
      },
      {
        where: {rating_id:rating_id}
      })
      return res.status(201).send({
        status: true,
        message: 'Edit Success'
      })
  
    } catch (err) {
      return res.status(500).send({
        status: false,
        message: err.message
      })
    }
  }

export const delete_rating_service = async (req, res) => {
    const {rating_id} = req.query
    try {
      await RatingModel.destroy({
        where: { rating_id: rating_id }
      })
      return res.status(201).send({
        status: true,
        message: 'Delete Success'
      })
  
    } catch (err) {
      return res.status(500).send({
        status: false,
        message: err.message
      })
    }
  }