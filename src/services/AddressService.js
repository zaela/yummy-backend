import db from '../config/index.js';
import AddressesModel from '../model/AddressModel.js';

export const get_address_service = async (req, res) => {
    try {
      // get restaurants
      const restaurants = await AddressesModel.findAll(
        {
          limit: Number(req.query.limit),
          offset: Number(req.query.offset * req.query.limit - req.query.limit),
          // where: where_clause,
          order: [['createdAt', 'DESC']],
        }
      )

      return res.status(200).send({
        status: true,
        data: restaurants
      })
  
    } catch (err) {
      return res.status(500).send({
        status: false,
        message: err.message
      })
    }
  }

export const add_address_service = async (req, res) => {
    const {user_id, detail_address, phone, house_no, city} = req.body
    try {
      await AddressesModel.create({
        user_id: user_id,
        detail_address: detail_address,
        phone:phone,
        house_no:house_no,
        city:city,
      })
      return res.status(201).send({
        status: true,
        message: 'Insert Success'
      })
    } catch (err) {
      return res.status(500).send({
        status: false,
        message: err.message
      })
    }
  }

export const detail_address_service = async (req, res) => {
    const {id} = req.query
    try {
      const address = await AddressesModel.findOne(
        {
          where: {id:id}
        }
      )
      return res.status(200).send({
        status: true,
        message: 'Insert Success',
        data:address
      })
    } catch (err) {
      return res.status(500).send({
        status: false,
        message: err.message
      })
    }
  }

export const edit_address_service = async (req, res) => {
    const {address_id, user_id, detail_address} = req.body
    try {
      await AddressesModel.update({
        user_id: user_id,
        detail_address: detail_address,
      },
      {
        where: {address_id:address_id}
      })
      return res.status(201).send({
        status: true,
        message: 'Edit Success'
      })
  
    } catch (err) {
      return res.status(500).send({
        status: false,
        message: err.message
      })
    }
  }

export const delete_address_service = async (req, res) => {
    const {address_id} = req.query
    try {
      await AddressesModel.destroy({
        where: { address_id: address_id }
      })
      return res.status(201).send({
        status: true,
        message: 'Delete Success'
      })
    } catch (err) {
      return res.status(500).send({
        status: false,
        message: err.message
      })
    }
  }