import bcrypt from 'bcrypt'
import jwt from 'jsonwebtoken';
import dotenv from 'dotenv';
import UsersModel from '../model/UsersModel.js';
import RefreshTokenModel from '../model/RefreshTokenModel.js';
import AddressesModel from '../model/AddressModel.js';
dotenv.config()

let refreshTokens = []

export const registration_service = async (req, res) => {
    try {
      const {username, email, password, role} = req.body
      // const {profile_picture} = req.file
      // const file_name = `${profile_picture.destination}/${profile_picture.filename}`
      // const salt = await bcrypt.genSalt(10) // Buat salt untuk acak password
      // const hashPassword = await bcrypt.hash(password, salt) // Acak password yang diinput user
      const hashPassword = await bcrypt.hash(password, 12) // Acak password yang diinput user
      // Cari apakah sudah ada user dengan email yang sama

      const alreadyExist = await UsersModel.findOne({
        where: { email: req.body.email }
      })
  
      // Jika sudah ada user, berhenti di sini dan berikan pesan
      if (alreadyExist) return res.status(200).send({
        status: false,
        message: 'User already exist'
      })
  
      // Payload untuk user baru
      const newUser = await UsersModel.create({
        username: username,
        email: email,
        password: hashPassword,
        role: role,
      })

      const accessToken = generateAccessToken(email) // Generate access token
      const refreshToken = jwt.sign(email, process.env.REFRESH_TOKEN_SECRET) // Generate refresh token
  
      return res.status(201).send({
        status: true,
        message: 'Register Success',
        data: {
          user:{
            id: newUser.id,
            name: newUser.username,
            email: newUser.email,
            role: newUser.role,
            profile_picture: "",
            accessToken: accessToken, 
            refreshToken: refreshToken,
          }
        }
      })
  
    } catch (error) {
  
      console.error(error)
      return res.status(500).send({
        status: false,
        message: 'An error occurred on registration_service'
      })
  
    }
  }

const generateAccessToken = (user) => {
  return jwt.sign({email:user}, process.env.ACCESS_TOKEN_SECRET, {expiresIn: '3h'})
}

const generateMobileAccessToken = (user) => {
  return jwt.sign({email:user}, process.env.ACCESS_TOKEN_SECRET)
}

export const login_service = async (req, res) => {
  let isMatched = false;
  const {email, password} = req.body
  const user = email

  const userData = await UsersModel.findOne({
    attributes: ['id','username', 'email', 'role','password'],
    where: { email: email }
  })

  if (userData) {
    // console.log("userData :", userData)
    isMatched = await bcrypt.compare(password, userData.password)
    if (!isMatched) {
      // console.log("isMatched :", isMatched)
      return res.status(401).send({
        status:false,
        message:"Invalid email or password"
      })
    }
  }

  const accessToken = generateAccessToken(user) // Generate access token
  const refreshToken = jwt.sign(user, process.env.REFRESH_TOKEN_SECRET) // Generate refresh token

  // const csrfToken = req.csrfToken(); // Generate CSRF token
  // res.cookie('XSRF-TOKEN', csrfToken); // Store the token in a cookie

  const userToken = await RefreshTokenModel.findOne({
    attributes: ['user_id'],
    where: { user_id: userData.id }
  })

  if (userToken) {
    await RefreshTokenModel.update({
      refresh_token: refreshToken,
    }, {
      where: { user_id: userData.id }
    })
  } else {
    await RefreshTokenModel.create({
      user_id: userData.id,
      refresh_token: refreshToken,
    })
  }
  refreshTokens.push(refreshToken)
  return res.status(200).send({
    status: true,
    message: 'Login Success',
    data:{
      id:userData.id,
      name:userData.username,
      email:userData.email,
      role:userData.role,
      accessToken: accessToken, 
      refreshToken: refreshToken,
      csrfToken: "csrfToken",
    }
  })
}

export const login_mobile_service = async (req, res) => {
  const {email} = req.body
  const user = email

  // "id": 3,
  // "user_id": 16,
  // "phone": 32456766,
  // "house_no": "0876433",
  // "city": "Jakarta",
  // "detail_address": "jl doktor satrio jakarta selatan",

  try {
    const userData = await UsersModel.findOne({
      attributes: ['id','username', 'email', 'role', 'profile_picture', 'status'],
      include:[
        {
          model:AddressesModel,
          attributes: ['id','phone', 'house_no', 'city', 'detail_address'],
        }
      ],
      where: { email: email }
    })
    const accessToken = generateMobileAccessToken(user)
    const refreshToken = jwt.sign(user, process.env.REFRESH_TOKEN_SECRET)
    const userToken = await RefreshTokenModel.findOne({
      attributes: ['user_id'],
      where: { user_id: userData.id }
    })
    if (userToken) {
      await RefreshTokenModel.update({
        refresh_token: refreshToken,
      }, {
        where: { user_id: userData.id }
      })
    } else {
      await RefreshTokenModel.create({
        user_id: userData.id,
        refresh_token: refreshToken,
      })
    }
    refreshTokens.push(refreshToken)
    return res.status(200).send({
      status: true,
      message: 'Login Success',
      data:{
        id:userData.id,
        user:userData,
        accessToken: accessToken, 
        refreshToken: refreshToken,
      }
    })
  } catch(error) {
    return res.status(500).send({
      status: false,
      message: 'Incorrect email or password',
      // message: error.message,
      data:null
    })
  }
}

export const refresh_token_service = async (req, res) => {
  const refreshToken = req.body.token
  if (refreshToken == null) return res.status(401)
  // if (!refreshTokens.includes(refreshToken)) return res.status(403)
  await RefreshTokenModel.findOne({
    where: { refresh_token: refreshToken }
  })
  jwt.verify(refreshToken, process.env.REFRESH_TOKEN_SECRET, (err, user) => {
    if (err) return res.status(403)
    const accessToken = generateAccessToken(user)
    res.status(201).send({accessToken:accessToken})
    // res.json({accessToken:accessToken})
  })
}

export const csrf_token_service = async (req, res) => {
  const csrfToken = req.csrfToken(); // Generate CSRF token
  res.cookie('XSRF-TOKEN', csrfToken); // Store the token in a cookie
  res.status(200).send({ csrfToken: csrfToken });
}



export const logout_service = async (req, res) => {
  refreshTokens = refreshTokens.filter(token => token !== req.body.token)
  return res.status(204)
}

export const get_users_service = async (req, res) => {
  let query = {
    order: [['createdAt', 'DESC']],
    attributes: [
      "id",
      "username",
      "email",
      "role",
      "profile_picture",
      "status",
    ]
  }

  if (req.query.limit) {
    query = {
      ...query,
      limit: Number(req.query.limit),
      offset: Number(req.query.offset * req.query.limit - req.query.limit),
    }
  }
  try {
    // get users
    const users = await UsersModel.findAll(query)

    return res.status(201).send({
      status: true,
      data: users
    })

  } catch (err) {
    return res.status(500).send({
      status: false,
      message: err.message
    })
  }
}

export const get_detail_users_service = async (req, res) => {
  const {id} = req.query
  try {
    // get users
    const users = await UsersModel.findOne(
      {
        where: {id:id},
        attributes: [
          "id",
          "username",
          "email",
          "role",
          "profile_picture",
          "status",
        ]
      }
    )

    return res.status(200).send({
      status: true,
      data: users
    })

  } catch (err) {
    return res.status(500).send({
      status: false,
      message: err.message
    })
  }
}

export const edit_users_service = async (file, body, res) => {
  const {name, location, status, user_id, id} = body
  let user_image = file
  let user = {
    user_id: user_id,
    name: name,
    location: location,
    status: status,
  }

  if (user_image) {
    const file_name = `${user_image.destination}/${user_image.filename}`
    user = {
      ...user,
      user_image: file_name,
    }
  }

  try {
    await UsersModel.update(user,
    {
      where: {id:id}
    })
    return res.status(200).send({
      status: true,
      message: 'Edit Success'
    })

  } catch (err) {
    return res.status(500).send({
      status: false,
      message: err.message
    })
  }
}

export const update_profile_picture_service = async (req, res) => {
  let user;
  let user_image = req.file
  let {id} = req.body

  if (user_image) {
    const file_name = `${user_image.destination}/${user_image.filename}`
    user = {
      profile_picture: file_name,
    }
  }

  console.log("user_image:", user)

  try {
    if (user_image) {
      await UsersModel.update(user,
      {
        where: {id:id}
      })
      return res.status(200).send({
        status: true,
        data: user,
        message: 'Edit Success'
      })
    } else {
      return res.status(500).send({
        status: false,
        message: 'No foto edited'
      })
    }

  } catch (err) {
    return res.status(500).send({
      status: false,
      message: err.message
    })
  }
}

export const edit_profile_service = async (req, res) => {
  // console.log("edit_profile_service :", req)
  const {username, email, user_id, address:detail_address, city, houseNumber:house_no, phoneNumber:phone} = req.body

  try {
    await UsersModel.update({username, email},
    {
      where: {id:user_id}
    })

    await AddressesModel.update({detail_address, city, house_no, phone},
    {
      where: {user_id:user_id}
    })

    const userData = await UsersModel.findOne({
      attributes: ['id','username', 'email', 'role', 'profile_picture', 'status'],
      include:[
        {
          model:AddressesModel,
          attributes: ['id','phone', 'house_no', 'city', 'detail_address'],
        }
      ],
      where: { id:user_id }
    })

    return res.status(200).send({
      status: true,
      message: 'Edit Success',
      data:userData
    })

  } catch (err) {
    return res.status(500).send({
      status: false,
      message: err.message
    })
  }
}

export const delete_users_service = async (req, res) => {
  const {id} = req.query
  // console.log("delete_users_service :", id)
  try {
    await UsersModel.destroy({
      where: { id: id }
    })
    return res.status(200).send({
      status: true,
      message: 'Delete Success'
    })

  } catch (err) {
    return res.status(500).send({
      status: false,
      message: err.message
    })
  }
}


