import { Op, Sequelize } from 'sequelize';
import db from '../config/index.js';
import ProductsModel from '../model/ProductsModel.js';
import TransactionsModel from '../model/TransactionsModel.js';
import ProductVariantsModel from '../model/ProductVariantsModel.js';
import ProductImagesModel from '../model/ProductImagesModel.js';
import UsersModel from '../model/UsersModel.js';
import AddressesModel from '../model/AddressModel.js';

export const get_transactions_service = async (req, res) => {
    try {
      // get transactions
      let include = {
        include: [
          {
            model: ProductsModel,
          },
        ],
      }

      let clause = {
        order: [['createdAt', 'DESC']],
      }

      const transactions = await TransactionsModel.findAll({
        ...include,
        ...clause
      })

      return res.status(201).send({
        status: true,
        data: transactions
      })
  
    } catch (err) {
      return res.status(500).send({
        status: false,
        message: err.message
      })
    }
  }

  export const get_transactions_mobile_service = async (req, res) => {
    try {
      // get transactions
      let include = {
        include: [
          {
            model: ProductsModel,
            include:[
              {
                model: ProductVariantsModel,
                attributes: ['variant_name', 'variant_image']
              },
              {
                model: ProductImagesModel,
                attributes: ['file_name']
              },
            ]
          },
          {
            model: UsersModel,
            include:[
              {
                model: AddressesModel,
              }
            ]
          },
        ],
      }

      let clause = {
        order: [['createdAt', 'DESC']],
      }

      if (req?.query?.status) {
        clause.where = {
          status: {
            [Op.eq]: req?.query?.status
          }
        }
      }

      const transactions = await TransactionsModel.findAll({
        ...include,
        ...clause
      })


      return res.status(201).send({
        status: true,
        data: transactions
      })
  
    } catch (err) {
      return res.status(500).send({
        status: false,
        message: err.message
      })
    }
  }

export const get_detail_transactions_service = async (req, res) => {
    try {
      // get transactions
      let include = {
        include: [
          {
            model: ProductsModel,
            include:[
              {
                model: ProductVariantsModel,
                attributes: ['variant_name', 'variant_image']
              },
              {
                model: ProductImagesModel,
                attributes: ['file_name']
              },
            ]
          },
          {
            model: UsersModel,
            // attributes: ['variant_name', 'variant_image']

          },
        ],
      }

      let clause = {
        order: [['createdAt', 'DESC']],
      }

      clause.where = {
        id: {
          [Op.eq]: req?.query?.id
        }
      }

      const transactions = await TransactionsModel.findOne({
        ...include,
        ...clause
      })

      return res.status(201).send({
        status: true,
        data: transactions
      })
  
    } catch (err) {
      return res.status(500).send({
        status: false,
        message: err.message
      })
    }
  }

export const get_detail_transactions_mobile_service = async (req, res) => {
    try {
      // get transactions
      let include = {
        include: [
          {
            model: ProductsModel,
            include:[
              {
                model: ProductVariantsModel,
                attributes: ['variant_name', 'variant_image']
              },
              {
                model: ProductImagesModel,
                attributes: ['file_name']
              },
            ]
          },
          {
            model: UsersModel,
            // attributes: ['variant_name', 'variant_image']

          },
        ],
      }

      let clause = {
        order: [['createdAt', 'DESC']],
      }

      clause.where = {
        id: {
          [Op.eq]: req?.query?.id
        }
      }


      const transactions = await TransactionsModel.findOne({
        ...include,
        ...clause
      })


      return res.status(201).send({
        status: true,
        data: transactions
      })
  
    } catch (err) {
      return res.status(500).send({
        status: false,
        message: err.message
      })
    }
  }

export const add_transactions_service = async (req, res) => {
    const {product_id, user_id, qty, status} = req.body
    try {
      await TransactionsModel.create({
        product_id: product_id,
        user_id: user_id,
        qty: qty,
        status: status,
      })
      // await db.query(`insert into restaurants (name, location, price_range) values ('${name}', '${location}', ${price_range})`)
      return res.status(201).send({
        status: true,
        message: 'Insert Success'
      })
  
    } catch (err) {
      return res.status(500).send({
        status: false,
        message: err.message
      })
    }
  }

export const checkout_transactions_service = async (req, res) => {
    const {product_id, user_id, quantity, total, status} = req.body
    try {
      await TransactionsModel.create({
        product_id: product_id,
        user_id: user_id,
        qty: quantity,
        status: status,
      })
      // await db.query(`insert into restaurants (name, location, price_range) values ('${name}', '${location}', ${price_range})`)
      return res.status(201).send({
        status: true,
        message: 'Insert Success'
      })
  
    } catch (err) {
      return res.status(500).send({
        status: false,
        message: err.message
      })
    }
  }

export const cancel_transactions_service = async (req, res) => {
    const {id, status} = req.body
    try {
      await TransactionsModel.update({
        status: status,
      },
      {
        where: {id:id}
      })

      return res.status(201).send({
        status: true,
        message: 'Canceled Success'
      })

    } catch (err) {
      return res.status(500).send({
        status: false,
        message: err.message
      })
    }
  }

  export const complete_transactions_service = async (req, res) => {
    const {id, status} = req.body
    try {
      await TransactionsModel.update({
        status: status,
      },
      {
        where: {id:id}
      })

      return res.status(201).send({
        status: true,
        message: 'Complete Success'
      })

    } catch (err) {
      return res.status(500).send({
        status: false,
        message: err.message
      })
    }
  }

export const edit_transactions_service = async (req, res) => {
    const {transaction_id, product_id, user_id, qty, status} = req.body
    try {
      await TransactionsModel.update({
        product_id: product_id,
        user_id: user_id,
        qty: qty,
        status: status,
      },
      {
        where: {transaction_id:transaction_id}
      })
      return res.status(201).send({
        status: true,
        message: 'Edit Success'
      })
  
    } catch (err) {
      return res.status(500).send({
        status: false,
        message: err.message
      })
    }
  }

export const delete_transactions_service = async (req, res) => {
    const {id} = req.body
    try {
      await TransactionsModel.destroy({
        where: { id: id }
      })
      return res.status(201).send({
        status: true,
        message: 'Delete Success'
      })
  
    } catch (err) {
      return res.status(500).send({
        status: false,
        message: err.message
      })
    }
  }