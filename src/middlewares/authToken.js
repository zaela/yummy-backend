import jwt from "jsonwebtoken";
import dotenv from "dotenv";

dotenv.config()

export const verifyToken = (req, res, next) => {
    const authHeader = req.headers['authorization']
    const token = authHeader
    if (token == null) return res.status(401)
    
    jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, user) => {
        
        if(err) return res.status(403).send(err)
        req.user = user
        next()
    })
}