import { add_rating_service, delete_rating_service, edit_rating_service, get_rating_service } from "../services/RatingService"

export const get_rating_controller = async (req, res) => {
    return await get_rating_service(req, res)
}

export const add_rating_controller = async (req, res) => {
    return await add_rating_service(req, res)
}

export const edit_rating_controller = async (req, res) => {
    return await edit_rating_service(req, res)
}

export const delete_rating_controller = async (req, res) => {
    return await delete_rating_service(req, res)
}