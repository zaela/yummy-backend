import { add_address_service, delete_address_service, detail_address_service, edit_address_service, get_address_service } from "../services/AddressService.js"

export const get_address_controller = async (req, res) => {
    return await get_address_service(req, res)
}

export const add_address_controller = async (req, res) => {
    return await add_address_service(req, res)
}

export const detail_address_controller = async (req, res) => {
    return await detail_address_service(req, res)
}

export const edit_address_controller = async (req, res) => {
    return await edit_address_service(req, res)
}

export const delete_address_controller = async (req, res) => {
    return await delete_address_service(req, res)
}