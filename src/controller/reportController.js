// import { add_address_service, delete_address_service, detail_address_service, edit_address_service, get_address_service } from "../services/AddressService.js"
// const pdf = require('html-pdf-node');
import pdf from "html-pdf-node";
export const report_pdf_controller = async (req, res) => {
    // return await get_address_service(req, res)
    const htmlContent = `<!DOCTYPE html>
    <html>
      <head>
        <meta charset="UTF-8" />
        <title>Invoice Struk Apotek</title>
        <style>
          body {
            width: 188px;
            height: 390px;
            padding-left: auto;
            padding-right: auto;
            margin-right: auto;
            margin-left: auto;
          }
          .nameklinik {
            font-size: 11px;
            font-family: "Calibri";
            color: #414141;
            font-style: normal;
            font-weight: 400;
            line-height: 80%;
          }
          .jenisklinik {
            font-size: 10px;
            font-family: "Calibri";
            color: #414141;
            font-weight: 400;
            line-height: 80%;
          }
          .alamatklinik {
            font-size: 8px;
            font-family: "Calibri";
            color: #414141;
            font-weight: 300;
          }
          .notlp {
            font-size: 8px;
            font-family: "Calibri";
            color: #414141;
            font-weight: 300;
          }
          .nofaktur {
            font-weight: 300;
            line-height: 80%;
            font-family: "Calibri";
            font-size: 7px;
            color: #414141;
          }
          .invoice-details {
            margin-top: 0px;
          }
    
          .invoice-details p {
            margin: 5px 0;
            font-size: 14px;
            font-weight: 300;
            font-family: "Calibri";
            font-size: 9px;
            color: #414141;
          }
    
          .row {
            display: flex;
          }
    
          .invoice-details .label {
            width: 70px;
            padding-top: 4px;
            padding-right: 20px;
            text-align: left;
            font-weight: 300;
            font-family: "Calibri";
            font-size: 9px;
            color: #414141;
          }
    
          .invoice-details .equal {
            width: 8px;
            padding-top: 4px;
            text-align: left;
            font-weight: 300;
            font-family: "Calibri";
            font-size: 9px;
            color: #414141;
          }
    
          .invoice-details .value {
            width: 150px;
            padding-top: 4px;
            padding-right: 20px;
            text-align: left;
            font-weight: 300;
            font-family: "Calibri";
            font-size: 9px;
            color: #414141;
          }
    
          .invoice-detail p {
            margin: 0px 0;
            font-size: 14px;
            font-weight: 400;
            font-family: "Calibri";
            font-size: 9px;
            color: #414141;
          }
    
          .invoice-detail .label2 {
            display: inline-block;
            width: auto;
            padding-top: 4px;
            padding-right: 5px;
            text-align: left;
            font-weight: 400;
            font-family: "Calibri";
            font-size: 9px;
            color: #414141;
          }
          .invoice-detail-total p {
            margin: 5px 0;
            font-size: 14px;
            font-weight: 400;
            font-family: "Calibri";
            font-size: 9px;
            color: #414141;
            display: flex;
            justify-content: space-between;
          }
    
          .invoice-detail {
            display: flex;
          }
    
          .invoice-detail-total .label2 {
            display: inline-block;
            width: 10px;
            padding-top: 4px;
            padding-right: 5px;
            text-align: left;
            font-weight: 400;
            font-family: "Calibri";
            font-size: 9px;
            color: #414141;
          }
          .invoice-detail-price {
            margin-top: 0px;
            display: flex;
            justify-content: space-between;
          }
    
          .invoice-detail-price div {
            margin: 0px 0;
            font-family: "Calibri";
            font-style: normal;
            font-weight: 300;
            font-size: 8px;
            line-height: 10px;
            color: #414141;
          }
    
          .invoice-detail-price .label {
            display: inline-block;
            width: 50px;
            padding-top: 4px;
            padding-right: 20px;
            text-align: left;
            font-weight: 300;
            font-family: "Calibri";
            font-size: 9px;
            color: #414141;
          }
    
          .invoice-detail-price .label2 {
            display: inline-block;
            width: 20px;
            padding-top: 4px;
            padding-right: 20px;
            text-align: left;
            font-weight: 300;
            font-family: "Calibri";
            font-size: 9px;
            color: #414141;
          }
    
          .invoice-detail-price-diskon {
            margin-top: 0px;
            display: flex;
            justify-content: space-between;
          }
    
          .invoice-detail-price-diskon div {
            margin: 0px 0;
            font-size: 14px;
            font-weight: 300;
            font-family: "Calibri";
            font-size: 9px;
            color: #414141;
          }
    
          .invoice-detail-price-diskon .label {
            display: inline-block;
            width: 80px;
            padding-top: 4px;
            padding-right: 20px;
            text-align: left;
            font-weight: 300;
            font-family: "Calibri";
            font-size: 9px;
            color: #414141;
          }
    
          .invoice-detail-price-diskon .label2 {
            display: inline-block;
            width: 20px;
            padding-top: 4px;
            padding-right: 20px;
            text-align: left;
            font-weight: 300;
            font-family: "Calibri";
            font-size: 9px;
            color: #414141;
          }
          .footer-price {
            font-family: "Calibri";
            font-size: 9px;
            font-weight: 300;
            font-style: normal;
          }
          .footer-message {
            font-family: "Calibri";
            font-weight: 400;
            font-size: 9px;
            color: #414141;
            font-style: normal;
          }
          .border {
            border: 1px dashed rgba(65, 65, 65, 0.3);
            width: 100%;
            height: 0px;
            color: #414141;
          }
          .reporting-details {
            margin-top: 5px;
          }
    
          .row {
            display: flex;
          }
    
          .reporting-details .value {
            width: 550px;
            padding-right: 20px;
            text-align: left;
            font-family: "Calibri";
            font-style: normal;
            font-weight: 400;
            font-size: 9px;
            line-height: 10px;
            color: #414141;
          }
    
          .reporting-details p {
            margin: 5px 0;
            text-align: left;
            font-family: "Calibri";
            font-style: normal;
            font-weight: 300;
            font-size: 14px;
            line-height: 17px;
            color: #414141;
            line-height: 15px;
            color: #414141;
          }
    
          .reporting-details .label {
            display: inline-block;
            width: 40px;
            font-family: "Calibri";
            font-style: normal;
            font-weight: 400;
            font-size: 9px;
            line-height: 10px;
            color: #414141;
          }
        </style>
      </head>
    
      <body>
        <div style="padding-top: 10px; text-align: center">
          <h1 class="nameklinik">{NamaKlinik}</h1>
          <h1 class="jenisklinik">{JenisKlinik}</h1>
          <h1 class="alamatklinik">{AlamatKlinik}</h1>
          <h1 class="notlp">{NoTlp}</h1>
        </div>
        <div style="padding-top: 10px"></div>
        <div style="border: 1px dashed; opacity: 0.3"></div>
    
        <div style="padding-top: 7px">
          <div class="invoice-details">
            <div class="row">
              <div class="label">NoFaktur</div>
              <div class="equal">:</div>
              <div class="value">12343434</div>
            </div>
            <div class="row">
              <div class="label">TglFaktur</div>
              <div class="equal">:</div>
              <div class="value">08/11/2022 10:54:20</div>
            </div>
            <div class="row">
              <div class="label">Pembayaran</div>
              <div class="equal">:</div>
              <div class="value">Tunai</div>
            </div>
            <div class="row">
              <div class="label">Kasir</div>
              <div class="equal">:</div>
              <div class="value">Arie Andreana</div>
            </div>
          </div>
        </div>
    
        <div style="padding-top: 10px"></div>
        <div class="border"></div>
    
        <div style="padding-top: 12px"></div>
    
        <div>
          <div class="reporting-details">
            <div class="row">
              <div class="label">1</div>
              <div class="value">Facial Acne Premium M&PBPA+MP Meradang</div>
            </div>
          </div>
    
          <div class="invoice-detail-price">
            <div>
              &nbsp;&nbsp;&nbsp;
              <span class="label" style="padding-left: 3px">123.675 x 2</span>
            </div>
            <div>
              <span class="label2">20.000.000</span>
            </div>
          </div>
        </div>
    
        <div>
          <div class="reporting-details">
            <div class="row">
              <div class="label">2</div>
              <div class="value">Kolton - Allopurinol</div>
            </div>
          </div>
    
          <div class="invoice-detail-price">
            <div>
              &nbsp;&nbsp;&nbsp;
              <span class="label" style="padding-left: 3px">123.675 x 2</span>
            </div>
            <div>
              <span class="label2">20.000.000</span>
            </div>
          </div>
          <div class="invoice-detail-price-diskon">
            <div>
              &nbsp;&nbsp;&nbsp;
              <span class="label" style="padding-left: 3px">Disc x 6.143.000</span>
            </div>
            <div>
              <span class="label2">-6.143.000</span>
            </div>
          </div>
        </div>
    
        <div>
          <div class="reporting-details">
            <div class="row">
              <div class="label">3</div>
              <div class="value">LIP SCRUB</div>
            </div>
          </div>
    
          <div class="invoice-detail-price">
            <div>
              &nbsp;&nbsp;&nbsp;
              <span class="label" style="padding-left: 3px">123.675 x 2</span>
            </div>
            <div>
              <span class="label2">20.000.000</span>
            </div>
          </div>
        </div>
    
        <div style="padding-top: 12px"></div>
        <div class="border"></div>
    
        <div
          style="padding-top: 10px; justify-content: space-between; display: flex"
        >
          <h1 class="footer-price">Sub Total</h1>
          <h1 class="footer-price">94.000</h1>
        </div>
    
        <div style="justify-content: space-between; display: flex">
          <h1 class="footer-price">Total Disc</h1>
          <h1 class="footer-price">1.600</h1>
        </div>
        <div style="justify-content: space-between; display: flex">
          <h1 class="footer-price">Total Dibebaskan</h1>
          <h1 class="footer-price">0</h1>
        </div>
        <div style="justify-content: space-between; display: flex">
          <h1 class="footer-price">Total Dijamin</h1>
          <h1 class="footer-price">0</h1>
        </div>
        <div style="justify-content: space-between; display: flex">
          <h1 class="footer-price">PPN 11%</h1>
          <h1 class="footer-price">10.164</h1>
        </div>
        <div style="justify-content: space-between; display: flex">
          <h1 class="footer-price">Biaya Admin</h1>
          <h1 class="footer-price">0</h1>
        </div>
        <div style="justify-content: space-between; display: flex">
          <h1 class="footer-price">Biaya Transaksi</h1>
          <h1 class="footer-price">106.064</h1>
        </div>
    
        <div style="padding-top: 10px"></div>
        <div style="border: 1px dashed; opacity: 0.3"></div>
    
        <div style="text-align: center">
          <h1 class="footer-message">Semoga Lekas Sembuh</h1>
        </div>
      </body>
    </html>
    `; // Replace with your actual HTML content

    // const options = { width:'800px', path: 'assets/report/output.pdf' };
    // const options = { format: 'A4', path: 'assets/report/output.pdf' };
    let options = {
      width: '48mm',
      height: 'auto',
      margin: { top: '3mm', bottom: '3mm', right: '3mm', left: '3mm' },
    };

    try {
        const pdfBuffer = await pdf.generatePdf({ content: htmlContent }, options);
        res.set('Content-Type', 'application/pdf');
        res.status(200).send(pdfBuffer);
    } catch (error) {
        res.status(500).send(error);
    }
}
