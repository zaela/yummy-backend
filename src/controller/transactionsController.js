import { add_transactions_service, cancel_transactions_service, checkout_transactions_service, complete_transactions_service, delete_transactions_service, edit_transactions_service, get_detail_transactions_mobile_service, get_detail_transactions_service, get_transactions_mobile_service, get_transactions_service } from "../services/TransactionsService.js"

export const get_transactions_controller = async (req, res) => {
    return await get_transactions_service(req, res)
}

export const get_transactions_mobile_controller = async (req, res) => {
    return await get_transactions_mobile_service(req, res)
}

export const get_detail_transactions_mobile_controller = async (req, res) => {
    return await get_detail_transactions_mobile_service(req, res)
}

export const add_transactions_controller = async (req, res) => {
    return await add_transactions_service(req, res)
}

export const get_detail_transactions_controller = async (req, res) => {
    return await get_detail_transactions_service(req, res)
}

export const checkout_transactions_controller = async (req, res) => {
    return await checkout_transactions_service(req, res)
}

export const cancel_transactions_controller = async (req, res) => {
    return await cancel_transactions_service(req, res)
}

export const complete_transactions_controller = async (req, res) => {
    return await complete_transactions_service(req, res)
}

export const edit_transactions_controller = async (req, res) => {
    return await edit_transactions_service(req, res)
}

export const delete_transactions_controller = async (req, res) => {
    return await delete_transactions_service(req, res)
}