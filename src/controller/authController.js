import { csrf_token_service, delete_users_service, edit_profile_service, edit_users_service, get_detail_users_service, get_users_service, login_mobile_service, login_service, logout_service, refresh_token_service, registration_service, update_profile_picture_service } from "../services/AuthService.js"

export const register_controller = async (req, res) => {
    return await registration_service(req, res)
}

export const login_controller = async (req, res) => {
    try {
        return await login_service(req, res)
    } catch (error) {
        return res.status(500).send({
            status: false,
            message: 'Login failed',
            data:null
        })
    }
}

export const login_mobile_controller = async (req, res) => {
    return await login_mobile_service(req, res)
}

export const refresh_token_controller = async (req, res) => {
    return await refresh_token_service(req, res)
}

export const csrf_token_controller = async (req, res) => {
    return await csrf_token_service(req, res)
}

export const logout_controller = async (req, res) => {
    return await logout_service(req, res)
}

export const get_users_controller = async (req, res) => {
    return await get_users_service(req, res)
}

export const get_detail_users_controller = async (req, res) => {
    return await get_detail_users_service(req, res)
}

export const edit_users_controller = async (req, res) => {
    return await edit_users_service(req.file, req.body, res)
}

export const edit_profile_controller = async (req, res) => {
    return await edit_profile_service(req, res)
}

export const update_profile_picture_controller = async (req, res) => {
    console.log("hhhhhh")
    return await update_profile_picture_service(req, res)
}

export const delete_users_controller = async (req, res) => {
    return await delete_users_service(req, res)
}