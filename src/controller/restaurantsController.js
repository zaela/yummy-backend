import {add_restaurants_service, delete_restaurants_service, edit_restaurants_service, get_detail_restaurants_service, get_restaurants_by_user_service, get_restaurants_service} from '../services/RestaurantsService.js'

export const get_restaurants_controller = async (req, res) => {
    return await get_restaurants_service(req, res)
}

export const get_restaurants_by_user_controller = async (req, res) => {
    return await get_restaurants_by_user_service(req, res)
}

export const get_detail_restaurants_controller = async (req, res) => {
    return await get_detail_restaurants_service(req, res)
}

export const add_restaurants_controller = async (req, res) => {
    return await add_restaurants_service(req.file, req.body, res)
}

export const edit_restaurants_controller = async (req, res) => {
    return await edit_restaurants_service(req.file, req.body, res)
}

export const delete_restaurants_controller = async (req, res) => {
    return await delete_restaurants_service(req, res)
}