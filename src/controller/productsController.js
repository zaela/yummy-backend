import { add_products_service, delete_products_service, edit_products_service, get_detail_product_service, get_products_service } from '../services/ProductsService.js'

export const get_products_controller = async (req, res) => {
    return await get_products_service(req, res)
}

export const get_detail_product_controller = async (req, res) => {
    return await get_detail_product_service(req, res)
}

export const add_products_controller = async (req, res) => {
    return await add_products_service(req, res)
}

export const edit_products_controller = async (req, res) => {
    return await edit_products_service(req, res)
}

export const delete_products_controller = async (req, res) => {
    return await delete_products_service(req, res)
}