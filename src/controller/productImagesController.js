import { add_product_images_service, delete_product_images_service, edit_product_images_service, get_product_images_service } from '../services/ProductImagesService.js'

export const get_product_images_controller = async (req, res) => {
    return await get_product_images_service(req, res)
}

export const add_product_images_controller = async (req, res) => {
    return await add_product_images_service(req, res)
}

export const edit_product_images_controller = async (req, res) => {
    return await edit_product_images_service(req, res)
}

export const delete_product_images_controller = async (req, res) => {
    return await delete_product_images_service(req, res)
}