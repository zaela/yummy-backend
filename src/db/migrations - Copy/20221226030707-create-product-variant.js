'use strict';

const product = require('../models/product');

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('ProductVariants', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      product_id: {
        allowNull: true,
        type: DataTypes.INTEGER,
        constraint: true,
        references: {
          model: product,
          key: 'id'
        },
        onDelete: 'CASCADE'
      },
      variant_name: {
        allowNull: false,
        type: DataTypes.STRING
      },
      variant_image: {
        allowNull: false,
        type: DataTypes.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('ProductVariants');
  }
};