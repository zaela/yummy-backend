'use strict';

const product = require('../models/product');
const user = require('../models/user');

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('Transactions', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      user_id: {
        allowNull: false,
        type: DataTypes.INTEGER,
        constraint: true,
        references: {
          model: user,
          key: 'id'
        },
      },
      product_id: {
        allowNull: false,
        type: DataTypes.INTEGER,
        constraint: true,
        references: {
          model: product,
          key: 'id'
        },
      },
      qty: {
        allowNull: false,
        type: Sequelize.INTEGER
      },
      note: {
        allowNull: true,
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('Transactions');
  }
};