'use strict';

const product = require('../models/product');
const transaction = require('../models/transaction');

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('Ratings', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      transaction_id: {
        allowNull: true,
        type: DataTypes.INTEGER,
        constraint: true,
        references: {
          model: transaction,
          key: 'id'
        },
      },
      product_id: {
        allowNull: true,
        type: DataTypes.INTEGER,
        constraint: true,
        references: {
          model: product,
          key: 'id'
        },
      },
      rate: {
        allowNull: false,
        type: DataTypes.INTEGER,
        validate:{
          min: 1, 
          max: 5,
        }
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('Ratings');
  }
};