'use strict';
import fetch from "node-fetch";
/** @type {import('sequelize-cli').Migration} */
// module.exports = {
 export default async function up(queryInterface, Sequelize) {
    await queryInterface.createTable('RatingProducts', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      transaction_id: {
        type: Sequelize.INTEGER
      },
      product_id: {
        type: Sequelize.INTEGER
      },
      rate: {
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  }
  export default async function down(queryInterface, Sequelize) {
    await queryInterface.dropTable('RatingProducts');
  }
// };