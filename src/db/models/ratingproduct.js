'use strict';
import fetch from "node-fetch";
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class RatingProduct extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  RatingProduct.init({
    transaction_id: DataTypes.INTEGER,
    product_id: DataTypes.INTEGER,
    rate: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'RatingProduct',
  });
  return RatingProduct;
};