'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class ProductVariant extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  ProductVariant.init({
    product_id: DataTypes.INTEGER,
    variant_name: DataTypes.STRING,
    variant_image: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'ProductVariant',
  });
  return ProductVariant;
};