import ProductImagesModel from "../model/ProductImagesModel.js"
import ProductsModel from "../model/ProductsModel.js"
import ProductVariantsModel from "../model/ProductVariantsModel.js"
import RatingModel from "../model/RatingModel.js"
import TransactionsModel from "../model/TransactionsModel.js"
import UsersModel from "../model/UsersModel.js"
import AddressesModel from "../model/AddressModel.js"

export class Relationship {
	constructor() {
		ProductVariantsModel.belongsTo(ProductsModel, { foreignKey: 'id' })
		ProductsModel.hasMany(ProductVariantsModel, { foreignKey: 'product_id' })
		ProductImagesModel.belongsTo(ProductsModel, { foreignKey: 'id' })
		ProductsModel.hasMany(ProductImagesModel, { foreignKey: 'product_id' })

		TransactionsModel.belongsTo(ProductsModel, { foreignKey: 'product_id' })
		ProductsModel.hasMany(TransactionsModel, { foreignKey: 'id' })
		
		// TransactionsModel.belongsTo(UsersModel, { foreignKey: 'user_id' })
		// UsersModel.hasMany(TransactionsModel, { foreignKey: 'id' })

		ProductsModel.hasMany(RatingModel, { foreignKey: 'product_id' })
		RatingModel.belongsTo(ProductsModel, { foreignKey: 'id' })
		
		TransactionsModel.belongsTo(UsersModel, { foreignKey: 'user_id' })
		UsersModel.hasMany(TransactionsModel, { foreignKey: 'id' })

		UsersModel.hasOne(AddressesModel, { foreignKey: 'user_id' })
		
	}
}