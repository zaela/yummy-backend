import { DataTypes } from 'sequelize';
import conn from '../config/sqlz.js';
import UsersModel from './UsersModel.js';

const AddressesModel = conn.define('addresses', {
	id: {
		allowNull: false,
		autoIncrement: true,
		primaryKey: true,
		type: DataTypes.INTEGER
	},
	user_id: {
		allowNull: false,
		type: DataTypes.INTEGER,
		constraint: true,
		references: {
			model: UsersModel,
			key: 'id'
		},
	},
	phone: {
		allowNull: true,
		type: DataTypes.INTEGER
	},
	house_no: {
		allowNull: true,
		type: DataTypes.STRING
	},
	city: {
		allowNull: true,
		type: DataTypes.STRING
	},
	detail_address: {
		allowNull: true,
		type: DataTypes.STRING
	},
}, {
	freezeTableName: true,
	underscored: true
});

// await AddressesModel.sync(); //untuk sinkronisasi jika ada struktur table yang berubah jika table belum ada
AddressesModel.sync({ alter: true }).then(r => r).catch(err => console.log(err)) //untuk sinkronisasi jika ada struktur table yang berubah
export default AddressesModel;
