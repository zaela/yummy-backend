import { DataTypes } from 'sequelize';
import conn from '../config/sqlz.js';
import ProductsModel from './ProductsModel.js';

const ProductImagesModel = conn.define('product_images', {
	id: {
		allowNull: false,
		autoIncrement: true,
		primaryKey: true,
		type: DataTypes.INTEGER
	},
	product_id: {
		allowNull: false,
		type: DataTypes.INTEGER,
		constraint: true,
		references: {
			model: ProductsModel,
			key: 'id'
		},
		onDelete: 'CASCADE'
	},
	file_name: {
		allowNull: false,
		type: DataTypes.STRING
	},
}, {
	freezeTableName: true,
	underscored: true
});

await ProductImagesModel.sync();
// ProductImagesModel.sync({ alter: true }).then(r => r).catch(err => console.log(err))
export default ProductImagesModel;
