import { DataTypes } from 'sequelize';
import conn from '../config/sqlz.js';
import RestaurantsModel from './RestaurantsModel.js';

const ProductsModel = conn.define('products', {
	id: {
		allowNull: false,
		autoIncrement: true,
		primaryKey: true,
		type: DataTypes.INTEGER
	},
	restaurant_id: {
		allowNull: false,
		type: DataTypes.INTEGER,
		constraint: true,
		references: {
			model: RestaurantsModel,
			key: 'id'
		},
		onDelete: 'CASCADE'
	},
	name: {
		allowNull: false,
		type: DataTypes.STRING
	},
	price: {
		allowNull: false,
		type: DataTypes.STRING
	},
	description: {
		allowNull: false,
		type: DataTypes.TEXT
	},
	status: {
		allowNull: false,
		type: DataTypes.BOOLEAN,
    	defaultValue: true,
	},
}, {
	freezeTableName: true,
	underscored: true
});

await ProductsModel.sync();
// ProductsModel.sync({ alter: true }).then(r => r).catch(err => console.log(err))
export default ProductsModel;