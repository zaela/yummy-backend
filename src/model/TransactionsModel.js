import { DataTypes } from 'sequelize';
import conn from '../config/sqlz.js';
import ProductsModel from './ProductsModel.js';
import UsersModel from './UsersModel.js';

const TransactionsModel = conn.define('transactions', {
	id: {
		allowNull: false,
		autoIncrement: true,
		primaryKey: true,
		type: DataTypes.INTEGER
	},
	product_id: {
		allowNull: true,
		type: DataTypes.INTEGER,
		constraint: true,
		references: {
			model: ProductsModel,
			key: 'id'
		},
	},
	user_id: {
		allowNull: true,
		type: DataTypes.INTEGER,
		constraint: true,
		references: {
			model: UsersModel,
			key: 'id'
		},
	},
	qty: {
		allowNull: true,
		type: DataTypes.INTEGER
	},
	status: {
		allowNull: false,
		type: DataTypes.STRING
	},
}, {
	freezeTableName: true,
	underscored: true
});

await TransactionsModel.sync();
// TransactionsModel.sync({ alter: true }).then(r => r).catch(err => console.log(err))
export default TransactionsModel;