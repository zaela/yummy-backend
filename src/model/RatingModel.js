import { DataTypes } from 'sequelize';
import conn from '../config/sqlz.js';
import ProductsModel from './ProductsModel.js';
import TransactionsModel from './TransactionsModel.js';

const RatingModel = conn.define('rating', {
	id: {
		allowNull: false,
		autoIncrement: true,
		primaryKey: true,
		type: DataTypes.INTEGER
	},
	transaction_id: {
		allowNull: true,
		type: DataTypes.INTEGER,
		constraint: true,
		references: {
			model: TransactionsModel,
			key: 'id'
		},
	},
	product_id: {
		allowNull: true,
		type: DataTypes.INTEGER,
		constraint: true,
		references: {
			model: ProductsModel,
			key: 'id'
		},
	},
	rate: {
		allowNull: false,
		type: DataTypes.INTEGER,
		validate:{
			min: 1, 
			max: 5,
		}
	}
}, {
	freezeTableName: true,
	underscored: true
});

await RatingModel.sync();
// RatingModel.sync({ alter: true }).then(r => r).catch(err => console.log(err))
export default RatingModel;
