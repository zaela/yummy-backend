import { DataTypes } from 'sequelize';
import conn from '../config/sqlz.js';
import UsersModel from './UsersModel.js';

const RestaurantsModel = conn.define('restaurants', {
	id: {
		allowNull: false,
		autoIncrement: true,
		primaryKey: true,
		type: DataTypes.INTEGER
	},
	user_id: {
		allowNull: true,
		type: DataTypes.INTEGER,
		constraint: true,
		references: {
			model: UsersModel,
			key: 'id'
		},
	},
	restaurant_image: {
		allowNull: false,
		type: DataTypes.STRING
	},
	name: {
		allowNull: false,
		type: DataTypes.STRING
	},
	location: {
		allowNull: false,
		type: DataTypes.STRING
	},
	status: {
		allowNull: false,
		type: DataTypes.BOOLEAN,
    	defaultValue: true,
	},
}, {
	freezeTableName: true,
	underscored: true
});

await RestaurantsModel.sync();
// RestaurantsModel.sync({ alter: true }).then(r => r).catch(err => console.log(err))
export default RestaurantsModel;
