import { DataTypes } from 'sequelize';
import conn from '../config/sqlz.js';

const UsersModel = conn.define('users', {
	id: {
		allowNull: false,
		autoIncrement: true,
		primaryKey: true,
		type: DataTypes.INTEGER
	},
	username: {
		allowNull: false,
		type: DataTypes.STRING
	},
	email: {
		allowNull: false,
		type: DataTypes.STRING
	},
	role: {
		allowNull: false,
		defaultValue:'customer',
		type: DataTypes.STRING
	},
	profile_picture: {
		allowNull: true,
		type: DataTypes.STRING
	},
	password: {
		allowNull: false,
		type: DataTypes.STRING
	},
	status: {
		allowNull: false,
		type: DataTypes.BOOLEAN,
    	defaultValue: true,
	},
}, {
	freezeTableName: true,
	underscored: true
});

// await UsersModel.sync();
await UsersModel.sync({ alter: true }).then(r => r).catch(err => console.log(err))
export default UsersModel;
