import { DataTypes } from 'sequelize';
import conn from '../config/sqlz.js';
import ProductsModel from './ProductsModel.js';

const ProductVariantsModel = conn.define('product_variants', {
	id: {
		allowNull: false,
		autoIncrement: true,
		primaryKey: true,
		type: DataTypes.INTEGER
	},
	product_id: {
		allowNull: true,
		type: DataTypes.INTEGER,
		constraint: true,
		references: {
			model: ProductsModel,
			key: 'id'
		},
		onDelete: 'CASCADE'
	},
	variant_name: {
		allowNull: false,
		type: DataTypes.STRING
	},
	variant_image: {
		allowNull: false,
		type: DataTypes.STRING
	},
}, {
	freezeTableName: true,
	underscored: true
});

await ProductVariantsModel.sync();
// ProductVariantsModel.sync({ alter: true }).then(r => r).catch(err => console.log(err))
export default ProductVariantsModel;
