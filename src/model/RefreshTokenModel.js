import { DataTypes } from 'sequelize';
import conn from '../config/sqlz.js';
import UsersModel from './UsersModel.js';

const RefreshTokenModel = conn.define('refresh_token', {
	id: {
		allowNull: false,
		autoIncrement: true,
		primaryKey: true,
		type: DataTypes.INTEGER
	},
	user_id: {
		allowNull: false,
		type: DataTypes.INTEGER,
		references: {
			model: UsersModel,
			key: 'id'
		}
	},
	refresh_token: {
		allowNull: false,
		type: DataTypes.STRING
	}
}, {
	freezeTableName: true,
	underscored: true
});

await RefreshTokenModel.sync();
// RefreshTokenModel.sync({ alter: true }).then(r => r).catch(err => console.log(err))
export default RefreshTokenModel;
