import path from 'path'

export const timestampFilename = (filename) => {
	var extension = path.extname(filename);
	// var nameWithoutExt = filename.split('.').slice(0, -1).join('.');
	var newFileName = `${Date.now()}${extension}`;
	// var newFileName = `${nameWithoutExt}_${Date.now()}${extension}`;
	return newFileName;
};
